<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

    'facebook' => [
        'client_id' => '523716967766943',
        'client_secret' => 'b81ea8eea2fd1273474e5d74539051bf',
        'redirect' => 'http://azorro.com/oAuth/facebook/callback',
    ],

    'twitter' => [
        'client_id' => 'npWdA6etrKEiflhbUMR5AGgST',
        'client_secret' => 'y6cPqzjuvArazYrz2sYSVWvtUiysSkwjVDXNAZYEawO6wI1nOY',
        'redirect' => 'http://azorro.com/oAuth/twitter/callback',
    ],

    'google' => [
        'client_id' => '973419391135-15drje7n27og9hp7uftcndtcqcon2j72.apps.googleusercontent.com',
        'client_secret' => 'KRyvz_rhHVyV8PGboj_w7nxc',
            'redirect' => 'http://azorro.com/oAuth/gmail/callback',
    ],


];
