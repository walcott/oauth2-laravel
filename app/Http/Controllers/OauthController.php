<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;

class OauthController extends Controller {

	public function facebookCallback(){
        $user = \Socialize::with('facebook')->user();
        $oauth_id = $user->getId();
        $count = User::where('oauth_id', '=', $oauth_id)->count();

        if ($count==0){

            $authUser = new User;

            $authUser->oauth_id =  $user->getId();
            $authUser->name = $user->getName();
            $authUser->email = $user->getEmail();
            $authUser->profile_pic =  $user->getAvatar();
            $authUser->save();
            Auth::loginUsingId($authUser->id);
        }
        else{
            $user = User::where('oauth_id', '=', $oauth_id)->get();
            Auth::loginUsingId($user[0]->id);
        }
        return redirect('/');
    }

    public function facebookRedirect(){
        return \Socialize::with('facebook')->redirect();


    }

    public function twitterCallback(){
        $user = \Socialize::with('twitter')->user();
        $oauth_id = $user->getId();
        $count = User::where('oauth_id', '=', $oauth_id)->count();

        if ($count==0){

            $authUser = new User;

            $authUser->oauth_id =  $user->getId();
            $authUser->name = $user->getName();
            $authUser->profile_pic =  $user->getAvatar();
            $authUser->save();
            Auth::loginUsingId($authUser->id);
        }
        else{
            $user = User::where('oauth_id', '=', $oauth_id)->get();
            Auth::loginUsingId($user[0]->id);
        }
        return redirect('/');
    }

    public function twitterRedirect(){
        return \Socialize::with('twitter')->redirect();


    }

    public function gmailCallback(){
        $user = \Socialize::with('google')->user();
        
        $oauth_id = $user->getId();
        $count = User::where('oauth_id', '=', $oauth_id)->count();

        if ($count==0){

            $authUser = new User;

            $authUser->oauth_id =  $user->getId();
            $authUser->name = $user->getName();
            $authUser->email = $user->getEmail();
            $authUser->profile_pic =  $user->getAvatar();
            $authUser->save();
            Auth::loginUsingId($authUser->id);
        }
        else{
            $user = User::where('oauth_id', '=', $oauth_id)->get();
            Auth::loginUsingId($user[0]->id);
        }
        return redirect('/');
    }

    public function gmailRedirect(){
        return \Socialize::with('google')->redirect();


    }

}
