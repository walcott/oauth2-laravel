<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\User;
//Route::get('/', 'WelcomeController@index');
Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::group(['prefix'=>'oAuth'], function(){
    Route::group(['prefix'=>'facebook'], function(){
        get('callback', 'OauthController@facebookCallback');
        get('redirect', 'OauthController@facebookRedirect');
    });

    Route::group(['prefix'=>'twitter'], function(){
        get('callback', 'OauthController@twitterCallback');
        get('redirect', 'OauthController@twitterRedirect');
    });

    Route::group(['prefix'=>'gmail'], function(){
        get('callback', 'OauthController@gmailCallback');
        get('redirect', 'OauthController@gmailRedirect');
    });


});

get('test', function(){
    $model = User::find(2);

    return dd($model);
});